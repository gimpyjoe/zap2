#-------------------------------------------------
#
# Project created by QtCreator 2014-06-01T22:20:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = wersjaokienkowa2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    modul2.cpp \
    modul1.cpp

HEADERS  += mainwindow.h \
    modul2.h \
    modul1.h

FORMS    += mainwindow.ui
