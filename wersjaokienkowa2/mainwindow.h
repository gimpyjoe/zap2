#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <string>
#include <ctime>
#include <cmath>
#include <sstream>
#include "modul1.h"
#include "modul2.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_lineEdit_2_returnPressed();

    void on_lineEdit_returnPressed();

    void on_pbStart_clicked();

    void on_cbMale_clicked(bool checked);

    void on_cbDuze_clicked(bool checked);

    void on_cbZnaki_clicked(bool checked);

    void on_cbCyfry_clicked(bool checked);

    void on_cbCzesc_clicked(bool checked);

    void on_leHaslo_returnPressed();

    void on_leCzesc_returnPressed();

private:
    Ui::MainWindow *ui;
    bool a,b,c,d,e;
    string czesc,odpowiedz,haslo,litery;
    clock_t start;
    lista*glowa;
    void czysc();
    void wypisz();

};

#endif // MAINWINDOW_H
