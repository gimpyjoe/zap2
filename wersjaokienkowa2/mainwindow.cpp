#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    glowa=new lista;
    glowa->data=0;
    glowa->next=NULL;
    a=0;b=0;c=0;d=0;e=0;

}

MainWindow::~MainWindow()
{
    delete glowa;

    delete ui;
}

void MainWindow::czysc(){
    lista*a1,*a2;
        a1=glowa;
        a2=glowa;

        while(a1->next!=NULL)
        {

            a1=a1->next;
            delete a2;
            a2=a1;
        }
        glowa=a2;
        a2->data=0;
        a2->next=NULL;
        odpowiedz="";
        haslo="";
        czesc="";
        litery="";
        start=0;

}

void MainWindow::wypisz()
{
    stringstream strumien;
    strumien<<"Twoje hasło to: "<<odpowiedz<<endl;
    strumien<<"Czas, który był potrzebny na złamanie hasła: "<< clock()-start<< "ms";


    string tekst = strumien.str();
    QString tekst_qt = tekst.c_str();
    ui->pteWyswietl->clear();
    ui->pteWyswietl->appendPlainText(tekst_qt);
}



void MainWindow::on_pbStart_clicked()
{
    czysc();

    pokolei(a,b,c,d,litery);
    haslo = ui->leHaslo->text().toStdString();
    if(e==1)czesc = ui->leCzesc->text().toStdString();
    start = clock();
    brute(glowa,odpowiedz,haslo,litery,e,czesc);

    wypisz();
    czysc();


}

void MainWindow::on_cbMale_clicked(bool checked)
{
    b=checked;
}

void MainWindow::on_cbDuze_clicked(bool checked)
{
    c=checked;
}

void MainWindow::on_cbZnaki_clicked(bool checked)
{
    d=checked;
}

void MainWindow::on_cbCyfry_clicked(bool checked)
{
    a=checked;
}

void MainWindow::on_cbCzesc_clicked(bool checked)
{
    e=checked;
}

void MainWindow::on_leHaslo_returnPressed()
{


}

void MainWindow::on_leCzesc_returnPressed()
{

}
