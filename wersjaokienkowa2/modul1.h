/**
 *\file modul1.h
 *\brief Plik nagłówkowy modułu \a modul1.
 *
 *Moduł \a modul1 zawiera definicje struktury lista
 *oraz funckje odpowiedzialne za podstawienie kolejnych znaków, części hasła
 *i sprawdzenie czy dany kolejny ciąg znaków
 *jest równoważny z podanym na poczatku działania programu hasłem.
 *
 *\see modul1.cpp
 */

#ifndef MODUL1_H
#define MODUL1_H
#include <iostream>
#include <string>
#include <cmath>
#include "modul2.h"

using namespace std;
/**
 *\brief Struktura przechowująca dana liczbową i wskażnik na kolejna komórke
 *
 */



struct lista
{
    int data;
    lista*next;
};


void zmien(int dl,lista* glowa);
void podstaw(string &odpwoiedz,string litery,lista* glowa);
void brute(lista* glowa,string &odpowiedz,string haslo,string litery,bool e,string czesc);
bool fragment(string &odpowiedz,string haslo,string czesc);


#endif // MODUL1_H
