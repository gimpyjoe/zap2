

#ifndef MODUL1_H
#define MODUL1_H
#include <iostream>
#include <string>
#include <cmath>
#include "modul2.h"

using namespace std;


struct lista
{
    int data;
    lista*next;
};


void zmien(int dl,lista* glowa);
void podstaw(string &odpwoiedz,string litery,lista* glowa);
void brute(lista* glowa,string &odpowiedz,string haslo,string litery,bool e,string czesc);
bool fragment(string &odpowiedz,string haslo,string czesc);


#endif // MODUL1_H
